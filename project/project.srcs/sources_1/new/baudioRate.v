module baudioRate
#(
    parameter   N = 10,       // number of bits in timer counter
                M = 651       // mod-M 652 para 100Mhz -- 9600bps
)
(
    input wire clk, reset,
	output wire tick
);

    reg	 [N-1:0] counter;
    wire [N-1:0] counter_next;
	
always @(posedge clk,posedge reset)
begin
    if (reset)
        counter <= 0;
	else
		counter <= counter_next;
end

assign counter_next = (counter == (M-1)) ? 0 : counter + 1;
assign tick = (counter == (M-1)) ? 1'b1 : 1'b0;

endmodule