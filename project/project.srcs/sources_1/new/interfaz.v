module interfaz
#( 
    parameter   byte_size = 7    // # data bits  
)
(
    input wire clk, reset, wr, rd, [byte_size:0] in_data,
	output wire empty, [byte_size:0] out_data  
);

// signal declaration
reg [byte_size:0] buf_reg, buf_next;
reg flag_reg, flag_next;

// FF & register
always @(posedge clk, posedge reset)
begin
    if(reset)
        begin
            buf_reg <= 0;
		    flag_reg <= 1'b0;
        end
    else
        begin
            buf_reg <= buf_next;
			flag_reg <= flag_next;
        end
 end
 
// next-state logic
always @*
begin
	buf_next = buf_reg;
	flag_next = flag_reg;
		
	if (wr)
		begin
			buf_next = in_data;
			flag_next = 1'b1;
		end
	else if (rd)
		flag_next = 1'b0;
end
  
// output logic
assign out_data = buf_reg;
assign empty = ~flag_reg;

endmodule