module transmisor
#( 
    parameter   byte_size = 7,      // # data bits  
				SB_TICK  =  16      // # ticks for stop bits  
)
( 
	input wire clk, reset, tx_start, s_tick, [byte_size:0] din,  // tx_start:arranque de transmision, s_tick: entrada de generador de baudios
	output reg tx_done_tick, 
	output wire tx  
); 

localparam idle 	= 4'b0001;
localparam start 	= 4'b0010;
localparam data 	= 4'b0100;
localparam stop 	= 4'b1000;  

reg [3:0] state_reg, state_next; 
reg [3:0] s_reg, s_next;  
reg [2:0] n_reg, n_next;  
reg [byte_size:0] b_reg, b_next;  
reg tx_reg, tx_next; 
 
always @(posedge clk, posedge reset)
begin   
	if (reset)  
		begin  
			state_reg	<=  idle;  
			s_reg  		<=  0;  
			n_reg  		<=  0;  
			b_reg  		<=  0;  
			tx_reg  	<=  1'b1;  
		end 
	else  
		begin  
			state_reg   <=  state_next;
			s_reg 		<=  s_next ;  
			n_reg 		<=  n_next;  
			b_reg 		<=  b_next ;  
			tx_reg 		<=  tx_next;  
		end
end
 
always @(*)  
begin 
	state_next 		= state_reg;  
	tx_done_tick 	= 1'b0;  
	s_next 			= s_reg;  
	n_next 			= n_reg;  
	b_next 			= b_reg;  
	tx_next 		= tx_reg; 
	case (state_reg)  
		idle : 
			begin  
				tx_next = 1'b1;  
				if (~tx_start)  
				begin  
					state_next 	= start;  
					s_next   	=  0 ;  
					b_next  	=  din;  
				end 
			end 
		start : 
			begin  
				tx_next = 1'b0;  
				if (s_tick)  
					if (s_reg == 15)  
						begin  
							state_next 	= data;  
							s_next 		= 0;  
							n_next 		= 0;  
						end 
					else  
						s_next = s_reg + 1 ;  
			end 
		data :  
			begin  
				tx_next = b_reg[0]; 
				if (s_tick)  
					if (s_reg == 15)  
						begin  
							s_next = 0 ;  
							b_next = b_reg >> 1;  
							if  (n_reg==(byte_size)) 
								state_next  =  stop ; 
							else  
								n_next = n_reg + 1; 
						end 
					else  
						s_next = s_reg + 1;  
			end 
		stop : 
			begin  
				tx_next = 1'b1;  
				if (s_tick)  
					if  (s_reg==(SB_TICK  - 1 ))  
						begin  
							state_next 		= idle;  
							tx_done_tick 	= 1'b1;  
						end 
					else  
						s_next = s_reg + 1; 
			end 
	endcase  
end 

assign tx = tx_reg;  //output  

endmodule