module receptor
#( 
    parameter   byte_size = 7,      // # data bits  
				SB_TICK  =  16      // # ticks for stop bits  
)
(
	input wire clk, reset, rx, s_tick,  
	output reg rx_done_tick,  
	output wire [byte_size:0] dout
);

localparam idle 	= 4'b0001;
localparam start 	= 4'b0010;
localparam data 	= 4'b0100;
localparam stop 	= 4'b1000;

reg [3:0] state_reg = idle;
reg [3:0] state_next = start;
reg [3:0] s_reg, s_next; 
reg [2:0] n_reg, n_next; 
reg [byte_size:0] b_reg, b_next;

always @(posedge clk,posedge reset) // Memory
	if (reset)  
		begin  
			state_reg 	<= idle;  
			s_reg 		<= 0;  
			n_reg 		<= 0;  
			b_reg 		<= 0;  
		end 
	else  
		begin  
			state_reg 	<= state_next;
			s_reg 		<= s_next;  
			n_reg 		<= n_next;  
			b_reg 		<= b_next;  
		end 

always @(*) // Next-state logic
begin
	state_next=state_reg; 
	rx_done_tick=1'b0;  
	s_next=s_reg;
	n_next=n_reg;  
	b_next=b_reg;  
	case (state_reg)
		idle : 
			begin
				if (rx==0)
					state_next = start;
					s_next=0;
			end
		start : 
			begin 
				if (s_tick==1)
					if(s_reg==7)
						begin
							s_next=0;
							n_next=0;
							state_next = data;
						end
					else
						s_next=s_reg+1;
			end
		data :
			begin
				if (s_tick==1)
					if(s_reg==15)
						begin
						s_next=0;
						b_next={rx,b_reg[byte_size:1]};
						if (n_reg==byte_size)
							state_next = stop;
						else
							n_next=n_reg+1;
						end	
					else
						s_next=s_reg+1;
			end
		stop : 
			begin 
				if (s_tick==1)
					if (s_reg==(SB_TICK-1))
						begin
						rx_done_tick=1'b1;
						state_next = idle;
						end
					else
						s_next=s_reg+1;
			end
		default : // Fault Recovery
			begin
				state_next = idle;
				s_next=0;
				n_next=0;
				b_next=0;
			end
	endcase	
end
	
assign dout = b_reg;

endmodule