module moduloUART
#( 
    parameter   byte_size = 7,                // # data bits  
				SB_TICK   =  16               // # ticks for stop bits  
)
(
    input wire clk, reset, rx_in,
    output wire tx_out, [byte_size:0] dout_a_din, [byte_size:0] out_data_a_din // entrada a tx -- 8 leds de la derecha - salida de rx -- 8 leds de la izquierda
);
 
wire [byte_size:0] r_data;
wire [byte_size:0] w_reg;
wire tick_a_s_tick, rx_done_a_wr, r_data_a_r_data_in, rd_a_tx_done_tick, rd_uart, rx_empty, wr_uart, alu_empty;

// instanciando el generador de baudio en 9600bps
baudioRate #
(
	.N(10),  // number of bits in timer counter
	.M(651)  // those values are valid for clk = 100Mhz
) 
generador_ticks
(
	.clk(clk),
	.reset(reset),
	.tick(tick_a_s_tick)
);

// instanciando el receptor
receptor #
(
	.byte_size(byte_size),
	.SB_TICK(SB_TICK)
)
rx_1
(
	.clk(clk),
	.reset(reset),
	.rx(rx_in),
	.s_tick(tick_a_s_tick),
	.rx_done_tick(rx_done_a_wr),
	.dout(dout_a_din)
);

interfaz # 
( 
    .byte_size(byte_size)    
)
interfaz_rx
(
    .clk(clk) ,
	.reset(reset) ,    
	.wr(rx_done_a_wr) , 
    .rd(rd_uart),
    .in_data(dout_a_din),
    .empty(rx_empty), 
    .out_data(r_data)  
);

// instanciando la ALU
interfaz_alu#
( 
    .byte_size(byte_size)
 )
alu_1
(
    .r_data_in(r_data),
	.clk(clk),
	.reset(reset),
	.rx_empty_in(rx_empty),
	.wr_uart(wr_uart),
	.rd_uart(rd_uart),
	.w_reg(w_reg)
 );

// instanciando la interfaz
interfaz # 
( 
	.byte_size(byte_size)    
)
interfaz_tx
(
    .clk(clk) ,
	.reset(reset) ,    
	.wr(wr_uart) , 
	.rd(rd_a_tx_done_tick),
	.in_data(w_reg),
	.empty(alu_empty), 
	.out_data(out_data_a_din)  
);

// instanciando el transmisor
transmisor
#( 
	.byte_size(byte_size),     //  # data bits  
	.SB_TICK (SB_TICK)         //  # ticks for stop bits  
)
tx_1
( 
    .clk(clk), 
	.reset(reset),  
	.tx_start(alu_empty),
	.s_tick(tick_a_s_tick),  
	.din(out_data_a_din),  
	.tx_done_tick(rd_a_tx_done_tick), 
	.tx(tx_out)
); 

endmodule