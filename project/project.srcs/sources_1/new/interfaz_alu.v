module interfaz_alu
#( 
	parameter   byte_size 	= 7
)
(
	input wire [byte_size:0] r_data_in,
	input wire clk, reset, rx_empty_in,
	output reg wr_uart, rd_uart,
	output wire [byte_size:0] w_reg
 );

localparam hacer_A 		= 4'b0001;
localparam hacer_B 		= 4'b0010;
localparam hacer_i_oper = 4'b0100;
localparam resultado 	= 4'b1000;

reg signed [byte_size:0] datoA_reg, datoA_next;
reg signed [byte_size:0] datoB_reg, datoB_next;
reg [5:0] i_oper_reg, i_oper_next;
reg [3:0] state_reg 	= hacer_A;
reg [3:0] state_next 	= hacer_B;
reg [2:0] n_reg, n_next;

ALU alu_1 
(	
	.i_datoA(datoA_reg), .i_datoB(datoB_reg),
	.i_oper(i_oper_reg),
	.o_aluResult(w_reg)
);

always @(posedge clk, posedge reset)
begin
	if(reset)
		begin
			n_reg<=0;
			datoA_reg<= 8'b00000000;
			datoB_reg<=8'b00000000;
			i_oper_reg<=5'b00000;
			state_reg <= hacer_A;
		end
	else
		begin
			n_reg<=n_next;
			datoA_reg<= datoA_next;
			datoB_reg<=datoB_next;
			i_oper_reg<=i_oper_next;
			state_reg <= state_next;
		end
end
 
always @(*) 
begin
	state_next 	= state_reg;  
	n_next 		= n_reg;
	rd_uart 	= 1'b0;
	wr_uart 	= 1'b0;
	datoA_next 	= datoA_reg;
	datoB_next 	= datoB_reg;
	i_oper_next = i_oper_reg;
	
	case (state_reg)
		hacer_A:
				if(~rx_empty_in)
					begin
						datoA_next=r_data_in;
						rd_uart=1'b1;
						state_next=hacer_B;
					end
		hacer_B:	
				if(~rx_empty_in)
					begin
						datoB_next=r_data_in;
						//datoA_r=datoA_reg;
						rd_uart=1'b1;
						state_next=hacer_i_oper;
					end	
		hacer_i_oper: 
				if(~rx_empty_in)
					begin
						i_oper_next=r_data_in;
						//datoB_r=datoB_reg;
						rd_uart=1'b1;
						state_next=resultado;
					end
		resultado:	 
				begin
					//i_oper_r=i_oper_reg;
					wr_uart=1'b1;
					state_next=hacer_A;		
				end
		default: 	// Fault Recovery
				begin
					state_next = hacer_A;
					n_next=0;
				end
	endcase
end
endmodule