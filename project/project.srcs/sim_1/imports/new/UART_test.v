`timescale 1ns / 1ps

module UART_test;

	// Inputs
	reg clk;
	reg rx_in;
	reg reset;

	// Outputs
	wire tx_out;
	wire [7:0] dout_a_din;
	wire [7:0] out_data_a_din;

	// Instantiate the Unit Under Test (UUT)
	moduloUART uut (
		.clk(clk), 
		.rx_in(rx_in), 
		.reset(reset),   
		.tx_out(tx_out),
		.dout_a_din(dout_a_din), //salida de rx
	    .out_data_a_din(out_data_a_din) //entrada a tx
	);

	initial begin
	// Initialize Inputs
		clk = 0;
		rx_in = 1;
		reset = 1;
		#1
		reset = 0;
		#19;
      
		/** se�al de entrada S+dato+P
		 datoA=00101010 (42)d **/
		rx_in=0; //S bit de start
		#104167
			rx_in=0;	//D0 LSB
		#104167
			rx_in=1;	//D1
		#104167
			rx_in=0;	//D2
		#104167
			rx_in=1;	//D3
		#104167
			rx_in=0;	//D4
		#104167
			rx_in=1;	//D5
		#104167
			rx_in=0;	//D6
		#104167
			rx_in=0; //D7 MSB
		#104167
			rx_in=1;	//P bit de stop
		
		#305000	// tiempo entre cada dato
		
	
		/** se�al de entrada S+dato+P
		 datoB=00001010 (10)d **/
		rx_in=0; //S bit de start
		#104167
			rx_in=0;	//D0 LSB
		#104167
			rx_in=1;	//D1
		#104167
			rx_in=0;	//D2
		#104167
			rx_in=1;	//D3
		#104167
			rx_in=0;	//D4
		#104167
			rx_in=0;	//D5
		#104167
			rx_in=0;	//D6
		#104167
			rx_in=0; //D7 MSB
		#104167
			rx_in=1;	//P bit de stop
			
		#305000	// tiempo entre cada dato
		
		/** se�al de entrada S+dato+P
		 i_oper=00100000 (suma)**/
		rx_in=0; //S bit de start
		#104167
			rx_in=0;	//D0 LSB
		#104167
			rx_in=0;	//D1
		#104167
			rx_in=0;	//D2
		#104167
			rx_in=0;	//D3
		#104167
			rx_in=0;	//D4
		#104167
			rx_in=1;	//D5
		#104167
			rx_in=0;	//D6
		#104167
			rx_in=0; //D7 MSB
		#104167
			rx_in=1;	//P bit de stop

	end
	
	always begin //clock de la placa 100Mhz-> (#5)
		#10 clk=~clk; //50Mhz
	end
 
endmodule

