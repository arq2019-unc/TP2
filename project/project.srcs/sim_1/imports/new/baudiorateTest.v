`timescale 1ns / 1ps

module baudiorateTest;

	// Inputs
	reg clk;
	reg reset;
	// Outputs
	wire tick;
	// Instantiate the Unit Under Test (UUT)
	baudioRate #
  	(
		.N(9),
		.M(326)	//parametros del generador de ticks para 50Mhz
	)
  	uut(
		.clk(clk), 
		.reset(reset), 
		.tick(tick)
	);

	initial begin
		$dumpfile("dump.vcd");
        $dumpvars;  
		// Initialize Inputs
		clk = 0;
		reset = 1;
		#1
		reset = 0;		       		 
		// Add stimulus here
        #10000
        $finish;
	end
   
	always begin //clock de la placa 50Mhz (periodo de 20ns, cambia cada 10)
		#10 clk=~clk;
	end	
endmodule

