`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   01:10:03 10/02/2017
// Design Name:   moduloRX
// Module Name:   E:/Facultad/Arquitectura de Computadoras/Practicos/interfaz_RX/moduloRX_test.v
// Project Name:  interfaz_RX
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: moduloRX
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module moduloRX_test;

	// Inputs
	reg clk;
	reg rx_in;
	reg reset;
	reg rd_uart;

	// Outputs
	wire rx_empty;
	wire [7:0] r_data;

	// Instantiate the Unit Under Test (UUT)
	moduloRX uut (
		.clk(clk), 
		.rx_in(rx_in), 
		.reset(reset), 
		.rd_uart(rd_uart), 
		.rx_empty(rx_empty),
		.r_data(r_data)
	);

	initial begin
		clk = 0;
		rd_uart = 0;
		rx_in = 1;
		reset = 1;
		#1
		reset = 0;
		#9
		/** se�al de entrada S+datos+P
		 datos=01100101 "e"
		 S=0,D0=1,D1=0,D2=1,D3=0,D4=0,D5=1,D6=1,D7=0,P=1**/
		rx_in=0; //S bit de start
		#104167
			rx_in=1;	//D0 LSB
		#104167
			rx_in=0;	//D1
		#104167
			rx_in=1;	//D2
		#104167
			rx_in=0;	//D3
		#104167
			rx_in=0;	//D4
		#104167
			rx_in=1;	//D5
		#104167
			rx_in=1;	//D6
		#104167
			rx_in=0; //D7 MSB
		#104167
			rx_in=1;	//P bit de stop
		#104167
			//rd_uart = 1;
	//	#20
			rd_uart = 0;
	end
   
	always begin //clock de la placa 50Mhz
		#10 clk=~clk;
	end
	

      
endmodule

