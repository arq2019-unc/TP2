`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   21:27:06 10/03/2017
// Design Name:   placa
// Module Name:   E:/Facultad/Arquitectura de Computadoras/Practicos/interfaz_RX/alu_con_interfaz_test.v
// Project Name:  interfaz_RX
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: placa
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module alu_con_interfaz_test;

	// Inputs
	reg [7:0] r_data_in;
	reg clk;
	reg reset;
	reg rx_empty_in;
	reg tx_full;

	// Outputs
	wire wr_uart;
	wire rd_uart;
	wire [7:0] w_reg;
	wire [7:0] datoA_r;
	wire [7:0] datoB_r;
	wire [7:0] operacion_r;
	// Instantiate the Unit Under Test (UUT)
	placa uut (
		.r_data_in(r_data_in), 
		.clk(clk), 
		.reset(reset), 
		.rx_empty_in(rx_empty_in), 
		.tx_full(tx_full), 
		.wr_uart(wr_uart), 
		.rd_uart(rd_uart), 
		.w_reg(w_reg),
		.datoA_r(datoA_r),
		.datoB_r(datoB_r),
		.operacion_r(operacion_r)
	);

	initial begin
		// Initialize Inputs
		r_data_in = 0;
		clk = 0;
		reset = 1;
		rx_empty_in = 1;
		tx_full = 0;
		#1
		reset=0;
		#19
		rx_empty_in = 0;
		r_data_in=8'h01;//carga datoA
		#20
		rx_empty_in = 1;
		#20
		rx_empty_in = 0;
		r_data_in=8'h08;//carga datoB
	 	#20
		rx_empty_in = 1;
		#20
		rx_empty_in = 0;
		r_data_in=6'b100000;//carga operacion suma
		#20
		rx_empty_in = 1;//-----------Nuevos datos
		#57 /// Tiempo que tardamos en teclear
		rx_empty_in = 0;
		r_data_in=8'hf0;//carga datoA
		#20
		rx_empty_in = 1;
		#20
		rx_empty_in = 0;
		r_data_in=8'hff;//carga datoB
		#20
		rx_empty_in = 1;
		#20
		rx_empty_in = 0;
		r_data_in=6'b100100;//carga operacion and
		#20
		rx_empty_in = 1;
	end
	
	always begin //clock de la placa 50Mhz
		#10 clk=~clk;
	end
   

	
endmodule

