`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   18:48:50 10/01/2017
// Design Name:   moduloTX
// Module Name:   E:/Facultad/Arquitectura de Computadoras/Practicos/transmisorUART/modulTX_test.v
// Project Name:  transmisorUART
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: moduloTX
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module modulTX_test;

	// Inputs
	reg clk;
	reg [7:0] din;
	reg tx_start;
	reg reset;

	// Outputs
	wire tx_done_tick;
	wire tx;

	// Instantiate the Unit Under Test (UUT)
	moduloTX uut (
		.clk(clk), 
		.din(din), 
		.tx_start(tx_start), 
		.reset(reset), 
		.tx_done_tick(tx_done_tick), 
		.tx(tx)
	);

	initial begin
		clk = 0;
		din = 0;
		tx_start = 0;
		reset = 1;
		#1;
		reset = 0;
		#9;
		tx_start = 1;
      din=8'b01100101;
		#30;
		tx_start = 0; //el bit de tx_start debe permanecer en alto un tiempo mayor al periodo del clk

	end
   
	
	always begin //clock de la placa 100Mhz
		#5 clk=~clk;
	end
	
endmodule

